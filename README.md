Usage
===

- v1.csv

```csv
id, name, age, team, pet
id112, bob, 25, yellow, cat
id113, sue, 27, orange, cat
```

- v2.csv

```csv
id, name, age, team, pet
id112, rob, 25, yellow, dog
id113, sue, 27, orange, cat
```

- command line

```bash
./datadiff --index-cols=0 --ignore-cols=1,2 v1.csv v2.csv
```

- output

```
row "1"
  col 1: bob, rob
  col 4: cat, dog
```

Immediate Goals
=
- quickly find the differences between two csv files
  - useful for regressions
- support multiple index columns
  - useful for complex tables
- support ignored columns

Script options
=
- --index-cols=1,2,..
- --ignore-cols=1,2,..
- --only-cols=1,2,..

Future Goals
=
- support json diffs

Documentation
=
- There are a few things that you need to do to use this tool (some of these steps will be moved to a script in future updates):
  - mkdir build
  - mkdir include
  - [download fast-cpp-csv-parser](https://github.com/ben-strasser/fast-cpp-csv-parser)
  - put csv.h in the `include` directory


Strategy
=
- Turn everything into a string
  - Support multiple data types in the future
- Use a single column as the key
  - Support multi-column keys in the future
