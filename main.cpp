#include <iostream>
#include <string>
#include <vector>
#include <unordered_map>

#include "include/csv.h"

#include "diff.hpp"
#include "types.hpp"

using namespace std;
int main(){

  Map m1;
  Map m2;
  io::CSVReader<5> in1("v1.csv");
  io::CSVReader<5> in2("v2.csv");
  in1.read_header(io::ignore_extra_column, "id", "name", "age", "team", "pet");
  std::string vendor; int size; double speed;
  string id, name, age, team, pet;
  while(in1.read_row(id, name, age, team, pet)){
    m1[id] = {id,name,age,team,pet};
    cout<< name << "," << age << endl;
  }
  in2.read_header(io::ignore_extra_column, "id", "name", "age", "team", "pet");
  while(in2.read_row(id, name, age, team, pet)){
    m2[id] = {id,name,age,team,pet};
    cout<< name << "," << age << endl;
  }

  test(m1, m2);
}

