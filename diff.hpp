#ifndef DIFF_HPP
#define DIFF_HPP

#include <string>
#include <vector>
#include <unordered_map>
#include <iostream>

#include "types.hpp"

using namespace std;

void printVector(vector<string> v) {
  for( auto it = v.begin(); it != v.end(); ++it) {
    cout << *it << ", ";
  }
}
void printMap(Map m1) {
  for(auto it = m1.begin(); it != m1.end(); ++it) {
    cout << it->first << endl;
    cout << "\t";
    printVector(it->second);
    cout << "\n";
  }
}

Map test(Map m1, Map m2) {
  Map diffs;
  for(auto it = m1.begin(); it != m1.end(); ++it) {
    cout << "HEY\n";
    cout << it->first << endl;
    vector<string> r2 = m2[it->first];
    cout << "r2[0]" << r2[0] << endl;
    int p2 = 0;
    for( auto jt = it->second.begin(); jt != it->second.end(); ++jt) {
      bool is_diff = (*jt != r2[p2]);
      cout << is_diff;
      cout << "\t" << *jt << ", ";
      cout << r2[p2] << endl;
      if(is_diff){
        diffs[it->first + "_" + to_string(p2)] = {*jt, r2[p2]};
      }
      ++p2;
    }
  }
  cout << "DIFFS:" << endl;
  printMap(diffs);
  return diffs;
}

#endif
